defmodule WkBoard.Boards.BoardServerTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, board_server} = start_supervised(WkBoard.Boards.BoardServer)
    %{board_server: board_server}
  end

  # TODO improve those test !
  test "init the board", %{board_server: board_server} do
    board = WkBoard.Boards.BoardServer.get_board(board_server)

    board_id = board.board_id
    name = board.name
    decks_count = board.decks |> Enum.count()

    assert board_id == "test"
    assert name == "Wk Board"
    assert decks_count == 2
  end

  test "move card", %{board_server: board_server} do
    WkBoard.Boards.BoardServer.move_card(board_server, "deck0", "deck1", "card0")
    board = WkBoard.Boards.BoardServer.get_board(board_server)

    decks_cards_count = board.decks |> Enum.map(fn(deck) -> Enum.count(deck.cards) end)

    assert decks_cards_count == [0, 1]
  end

end