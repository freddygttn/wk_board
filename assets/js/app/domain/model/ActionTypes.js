const PREFIX = "wkboard.actions";
export default {
  Generic: {
    Noop: `${PREFIX}.generic.noop`
  },
  Board: {
    InitBoard: `${PREFIX}.board.init`,
    MoveCard: `${PREFIX}.board.move_card`,
    PerformMoveCard: `${PREFIX}.board.perform_move_card`,
    CardMoved: `${PREFIX}.board.card_moved`
  },
  Socket: {
    Init: `${PREFIX}.socket.init`,
    Open: `${PREFIX}.socket.open`,
    Close: `${PREFIX}.socket.close`,
    Error: `${PREFIX}.socket.error`
  }
};
