export default {
  Offline: "SocketStatus.Offline",
  Connecting: "SocketStatus.Connecting",
  Online: "SocketStatus.Online"
};
