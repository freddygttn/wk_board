export default {
  Socket: {
    Open: "socket:open",
    Close: "socket:close",
    Error: "socket:error"
  },
  ChannelJoin: {
    Ok: "join:ok",
    Error: "join:error",
  },
  EventDelivery: {
    Ok: "event-delivery:ok",
    Error: "event-delivery:error",
    Timeout: "event-delivery:timeout",
  },
  Response: {
    Ok: "ok",
    Error: "error",
    Timeout: "timeout"
  },
  Board: {
    CardMove: "card:move",
    CardMoved: "card:moved"
  }
};
