import R from "ramda";
import ActionTypes from "../../model/ActionTypes";
import SocketStatus from "../../model/SocketStatus";

// ----------------------------------------------------------------------------
// Initial state
// ----------------------------------------------------------------------------
const InitialState = {
  status: SocketStatus.Offline
};

// ----------------------------------------------------------------------------
// Reducer
// ----------------------------------------------------------------------------
export default (state = InitialState, action) => {
  switch (action.type) {

  case ActionTypes.Socket.Init:
    return R.merge(state, { status: SocketStatus.Connecting });

  case ActionTypes.Socket.Open:
    return R.merge(state, { status: SocketStatus.Online });

  case ActionTypes.Socket.Close:
    return R.merge(state, { status: SocketStatus.Offline });

  case ActionTypes.Socket.Error:
    return R.merge(state, { status: SocketStatus.Offline });

  default:
    return state;

  }
};
