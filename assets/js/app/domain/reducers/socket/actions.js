import ActionTypes from "../../model/ActionTypes";

export const initSocket = () => ({
  type: ActionTypes.Socket.Init,
  payload: {}
});

export const socketOpen = () => ({
  type: ActionTypes.Socket.Open,
  payload: {}
});

export const socketClose = () => ({
  type: ActionTypes.Socket.Close,
  payload: {}
});

export const socketError = () => ({
  type: ActionTypes.Socket.Error,
  payload: {}
});
