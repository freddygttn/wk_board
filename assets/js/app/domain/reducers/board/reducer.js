import R from "ramda";
import ActionTypes from "../../model/ActionTypes";

// ----------------------------------------------------------------------------
// Initial state
// ----------------------------------------------------------------------------
const InitialState = {
  id: "",
  name: "",
  decks: []
};

// ----------------------------------------------------------------------------
// Reducer
// ----------------------------------------------------------------------------
export default (state = InitialState, action) => {
  switch (action.type) {

  case ActionTypes.Board.InitBoard:
    return action.payload.board;

  case ActionTypes.Board.PerformMoveCard:
    return moveToDeck(state, action.payload.fromDeckId, action.payload.toDeckId, action.payload.cardId, action.payload.index);

  case ActionTypes.Board.MoveCard:
    return moveToDeck(state, action.payload.fromDeckId, action.payload.toDeckId, action.payload.cardId, action.payload.index, true);

  case ActionTypes.Board.CardMoved:
    return moveToDeck(state, action.payload.fromDeckId, action.payload.toDeckId, action.payload.cardId, action.payload.index);

  default:
    return state;

  }
};

// ----------------------------------------------------------------------------
// State functions
// ----------------------------------------------------------------------------
/**
 * Move a card from one deck to another.
 * @param {*} state
 * @param {string} fromDeckId
 * @param {string} toDeckId
 * @param {string} cardId
 * @param {number} index
 */
export const moveToDeck = (state, fromDeckId, toDeckId, cardId, index = null, isMoving = false) => {
  // Preconditions

  // fromDeck is nil
  const fromDeck = R.find(R.propEq("id", fromDeckId))(state.decks);
  if (R.isNil(fromDeck)) {
    return state;
  }

  // toDeck is nil
  const toDeck = R.find(R.propEq("id", toDeckId))(state.decks);
  if (R.isNil(toDeck)) {
    return state;
  }

  // cardToMove is nil
  const cardToMove = R.clone( R.find(R.propEq("id", cardId))(fromDeck.cards) );
  if (R.isNil(cardToMove)) {
    return updateCardMoving(state, toDeckId, cardId, false);
  }

  // Move card logic
  let newDecks = state.decks;

  // fromDeck == toDeck
  if (R.equals(fromDeck, toDeck)) {
    newDecks = state.decks.map((deck) => {
      if (deck.id === fromDeckId) {
        return reorderOnly(deck, cardId, index);
      }
      else {
        return deck;
      }
    });
  }
  else {
    newDecks = state.decks.map((deck) => {
      if (deck.id === fromDeckId) {
        return R.merge(deck, { cards: deck.cards.filter((card) => card.id !== cardId) });
      }
      else if (deck.id === toDeckId) {
        const deckWithCard = R.merge(deck, { cards: deck.cards.concat(cardToMove) });
        return reorderOnly(deckWithCard, cardId, index);
      }
      else {
        return deck;
      }
    });
  }
  return updateCardMoving(R.merge(state, { decks: newDecks }), toDeckId, cardId, isMoving);
};

/**
 * Reorder a card within a deck.
 * @param {*} deck The deck to reorder
 * @param {*} cardId The cardId to move
 * @param {*} index The index to move to
 */
export const reorderOnly = (deck, cardId, index) => {
  let safeIndex = index;
  if (R.isNil(index) || index < 0 || index >= deck.cards.length) {
    safeIndex = deck.cards.length - 1;
  }
  const cardIndex = R.findIndex(R.propEq("id", cardId))(deck.cards);
  if (cardIndex < 0) {
    return deck;
  }

  if (safeIndex > cardIndex) {
    return R.merge(deck, {
      cards: deck.cards
        .slice(0, cardIndex)
        .concat(deck.cards.slice(cardIndex + 1, safeIndex + 1))
        .concat(deck.cards[cardIndex])
        .concat(deck.cards.slice(safeIndex + 1, deck.cards.length))
    });
  }
  else if (safeIndex < cardIndex) {
    return R.merge(deck, {
      cards: deck.cards
        .slice(0, safeIndex)
        .concat(deck.cards[cardIndex])
        .concat(deck.cards.slice(safeIndex, cardIndex))
        .concat(deck.cards.slice(cardIndex + 1, deck.cards.length))
    });
  }
  else {
    return deck;
  }
};

const updateCardMoving = (state, deckId, cardId, isMoving) => {
  // deck is nil
  const deck = R.find(R.propEq("id", deckId))(state.decks);
  if (R.isNil(deck)) {
    return state;
  }

  // cardToUpdate is nil
  const cardToUpdate = R.merge(
    R.clone( R.find(R.propEq("id", cardId))(deck.cards) ),
    { isMoving }
  );
  if (R.isNil(cardToUpdate)) {
    return state;
  }

  const newDecks = state.decks.map((deck) => {
    if (deck.id === deckId) {
      return R.merge(deck, { cards: deck.cards.map((card) => {if (card.id == cardId) return cardToUpdate; else return card;}) });
    }
    else {
      return deck;
    }
  });

  return R.merge(state, { decks: newDecks });
};
