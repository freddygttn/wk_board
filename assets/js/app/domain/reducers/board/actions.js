import ActionTypes from "../../model/ActionTypes";

export const initBoard = (board) => ({
  type: ActionTypes.Board.InitBoard,
  payload: {
    board
  }
});

export const performMoveCard = (fromDeckId, toDeckId, cardId, index) => ({
  type: ActionTypes.Board.PerformMoveCard,
  payload: {
    fromDeckId,
    toDeckId,
    cardId,
    index
  }
});

export const moveCard = (fromDeckId, toDeckId, cardId, index) => ({
  type: ActionTypes.Board.MoveCard,
  payload: {
    fromDeckId,
    toDeckId,
    cardId,
    index
  }
});

export const cardMoved = (fromDeckId, toDeckId, cardId, index) => ({
  type: ActionTypes.Board.CardMoved,
  payload: {
    fromDeckId,
    toDeckId,
    cardId,
    index
  }
});
