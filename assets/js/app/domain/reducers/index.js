import { combineReducers } from "redux";
import board from "./board/reducer";
import socket from "./socket/reducer";

export default combineReducers({
  board,
  socket
});
