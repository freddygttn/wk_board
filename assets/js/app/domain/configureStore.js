import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";
import reducer from "./reducers";
import epic from "./epics";

const epicMiddleware = createEpicMiddleware(epic);

export default function configureStore() {
  const store = createStore(
    reducer,
    applyMiddleware(epicMiddleware)
  );


  return store;
}
