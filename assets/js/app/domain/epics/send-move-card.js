import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

import ActionTypes from "../model/ActionTypes";
import { getBoardChannel } from "../../data/socket";

export default action$ => {
  return action$.ofType(ActionTypes.Board.PerformMoveCard)
    .map(action => {
      getBoardChannel().moveCard(action.payload.fromDeckId, action.payload.toDeckId, action.payload.cardId, action.payload.index);
      return {
        type: ActionTypes.Generic.Noop,
        payload: {}
      };
    });
};
