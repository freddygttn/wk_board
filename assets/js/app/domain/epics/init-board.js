import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

import ActionTypes from "../model/ActionTypes";
import SocketEvents from "../model/SocketEvents";
import { getBoardChannel } from "../../data/socket";
import { initBoard } from "../reducers/board/actions";

// Init board by joining its channel
export default action$ => {
  return action$.ofType(ActionTypes.Socket.Open)
    .switchMap((_action) => {
      return getBoardChannel().join()
        .filter((response) => response.event === SocketEvents.ChannelJoin.Ok)
        .map((response) => {
          return initBoard(response.payload.board);
        });
    });
};
