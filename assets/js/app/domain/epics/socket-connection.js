import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/map";

import ActionTypes from "../model/ActionTypes";
import SocketEvents from "../model/SocketEvents";
import { connect } from "../../data/socket";
import { socketOpen, socketError, socketClose } from "../reducers/socket/actions";

export default action$ => {
  return action$.ofType(ActionTypes.Socket.Init)
    .switchMap(_action => {
      return connect()
        .map(response => {
          switch (response.event) {
          case SocketEvents.Socket.Open:
            return socketOpen();
          case SocketEvents.Socket.Close:
            return socketClose();
          case SocketEvents.Socket.Error:
            return socketError();
          }
        });
    });
};
