import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

import ActionTypes from "../model/ActionTypes";
import SocketEvents from "../model/SocketEvents";
import { getBoardChannel } from "../../data/socket";
import { cardMoved } from "../reducers/board/actions";


// Watch board events after the board is initialized (after its channel is open)
export default action$ => {
  return action$.ofType(ActionTypes.Board.InitBoard)
    .switchMap((_action) => {
      return getBoardChannel().asObservable()
        .filter((response) => response.event === SocketEvents.Board.CardMoved)
        .map((response) => {
          return cardMoved(response.payload.from_deck_id, response.payload.to_deck_id, response.payload.card_id, response.payload.card_index);
        });
    });
};
