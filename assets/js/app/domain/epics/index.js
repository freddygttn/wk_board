import { combineEpics } from "redux-observable";
import initBoard from "./init-board";
import notifyCardMoved from "./notify-card-moved";
import sendMoveCard from "./send-move-card";
import socketConnection from "./socket-connection";

export default combineEpics(
  initBoard,
  notifyCardMoved,
  sendMoveCard,
  socketConnection
);
