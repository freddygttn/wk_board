import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { moveCard } from "../../domain/reducers/board/actions";

import Board from "../components/board/Board";

class BoardContainer extends Component {
  componentDidMount() {
  }

  render() {
    return (
      <Board
        name={this.props.board.name}
        decks={this.props.board.decks}
        status={this.props.status} />
    );
  }
}

BoardContainer.propTypes = {
  board: PropTypes.shape({
    name: PropTypes.string.isRequired,
    decks: PropTypes.array.isRequired
  }).isRequired,
  status: PropTypes.string.isRequired,
  onCardMove: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    board: state.board,
    status: state.socket.status
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCardMove: (fromDeckId, toDeckId, cardId) => {
      dispatch(moveCard(fromDeckId, toDeckId, cardId));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BoardContainer);
