import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { moveCard, performMoveCard } from "../../domain/reducers/board/actions";

import Deck from "../components/deck/Deck";

/**
 * Deck wrapper to inject actions
 * @param {*} props
 */
const BoardDeck = ({deck, dispatch}) => {
  const onCardDrop = (fromDeckId, toDeckId, cardId, index) => {
    dispatch(performMoveCard(fromDeckId, toDeckId, cardId, index));
  };
  const onCardMove = (fromDeckId, toDeckId, cardId, index) => {
    dispatch(moveCard(fromDeckId, toDeckId, cardId, index));
  };
  return (
    <Deck deck={deck} onCardDrop={onCardDrop} onCardMove={onCardMove}></Deck>
  );
};

BoardDeck.propTypes = {
  deck: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    cards: PropTypes.array.isRequired,
  }).isRequired,
  dispatch: PropTypes.func.isRequired
};

export default connect()(BoardDeck);
