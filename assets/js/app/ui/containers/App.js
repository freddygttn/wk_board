import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import MainNav from "../components/nav/MainNav";
import BoardContainer from "./BoardContainer";

import { initSocket } from "../../domain/reducers/socket/actions";

/**
 * Top-level component
 */
class App extends React.Component {
  componentDidMount() {
    this.props.dispatch(initSocket());
  }

  render() {
    return (
      <div className="App">
        <MainNav />
        <BoardContainer />
      </div>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const ConnectedApp = connect()(App);
export default DragDropContext(HTML5Backend)(ConnectedApp);
