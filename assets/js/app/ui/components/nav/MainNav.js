import React from "react";

const MainNav = () => (
  <nav className="navbar is-primary" role="navigation" aria-label="main navigation">
    <div className="navbar-brand">
      <a className="navbar-item" href="">
        WK
      </a>
      <div className="navbar-burger">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </nav>
);

export default MainNav;
