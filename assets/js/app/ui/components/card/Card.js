import React from "react";
import PropTypes from "prop-types";
import { DragSource } from "react-dnd";
import DraggableTypes from "../../../domain/model/DraggableTypes";

import styles from "./Card.scss";

// Source specifications
const cardSource = {
  beginDrag(props) {
    return {
      // Feels kinda hacky... But hey, its 11pm.
      originalDeckId: props.deckId,
      deckId: props.deckId,
      cardId: props.card.id,
      index: props.index
    };
  }
};

function collectDrag(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

/**
 * A card in our app
 * @param {*} props
 */
class Card extends React.Component {
  render() {
    let cardClassNames = styles.Card + " box";
    if (this.props.card.isMoving) {
      cardClassNames += " " + styles.isDragging;
    }
    return (this.props.connectDragSource(
      <div ref={(rootEl) => this.rootEl = rootEl} className={cardClassNames}>
        <article className="media">
          <div className="media-left">
            <div className="image is-64x64 is-square">
              <img src={this.props.card.pictureUrl} alt="Image" />
            </div>
          </div>
          <div className="media-content">
            <div className="content">
              <p>
                <strong>{this.props.card.name}</strong>
                <br/>
                {this.props.card.content}
              </p>
            </div>
            <nav className="level is-mobile">
              <div className="level-left">
                <span className="level-item" style={{marginRight: "1.5rem"}}>
                  <span className="tag is-primary">{this.props.card.stats.rating}</span>
                </span>
                <span className="level-item" style={{marginRight: "1.5rem"}}>
                  <span className="icon is-small has-text-grey-light"><i className="fas fa-thumbs-up"></i>{this.props.card.stats.likes}</span>
                </span>
                <span className="level-item" style={{marginRight: "1.5rem"}}>
                  <span className="icon is-small has-text-grey-light"><i className="fas fa-comment"></i>{this.props.card.stats.msgs}</span>
                </span>
                <span className="level-item">
                  <span className="icon is-small has-text-grey-light"><i className="fas fa-envelope"></i>{this.props.card.stats.emails}</span>
                </span>
              </div>
            </nav>
          </div>
        </article>
      </div>
    ));
  }
}

Card.propTypes = {
  deckId: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  card: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    content: PropTypes.string,
    pictureUrl: PropTypes.string,
    stats: PropTypes.shape({
      rating: PropTypes.number,
      likes: PropTypes.number,
      msgs: PropTypes.number,
      emails: PropTypes.number
    }),
    isMoving: PropTypes.bool
  }).isRequired,
  connectDragSource: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
};

export default DragSource(DraggableTypes.CARD, cardSource, collectDrag)(Card);
