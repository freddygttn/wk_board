import React from "react";
import PropTypes from "prop-types";

import SocketStatus from "../../../domain/model/SocketStatus";

function getStatusClassName(status) {
  switch (status) {
  case SocketStatus.Offline:
    return "has-text-danger";
  case SocketStatus.Connecting:
    return "has-text-warning";
  case SocketStatus.Online:
    return "has-text-success";
  default:
    return "has-text-danger";
  }
}

const Indicator = ({status}) => {
  const indicatorClassNames = getStatusClassName(status);
  return (
    <div className={indicatorClassNames}>
      <span className="icon">
        <i className="fas fa-circle"></i>
      </span>
    </div>
  );
};

Indicator.propTypes = {
  status: PropTypes.string.isRequired
};

export default Indicator;
