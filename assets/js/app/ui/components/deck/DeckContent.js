import React from "react";
import PropTypes from "prop-types";

import Card from "../card/Card";

import styles from "./DeckContent.scss";

/**
 * Content of a Deck. Contains cards.
 * @param {*} props
 */
const DeckContent = ({ id, cards, isOver }) => {

  let deckContentClassNames = styles.DeckContent + " card-content";
  if (isOver) {
    deckContentClassNames += " " + styles.isOver;
  }

  return (
    <div className={deckContentClassNames}>
      <div className="content">
        {cards.map((card, index) => (
          <Card key={index} index={index} deckId={id} card={card}/>
        ))}
      </div>
    </div>
  );
};

DeckContent.propTypes = {
  id: PropTypes.string.isRequired,
  cards: PropTypes.array.isRequired,
  isOver: PropTypes.bool.isRequired,
};

export default DeckContent;
