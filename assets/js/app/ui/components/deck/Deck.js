import React from "react";
import PropTypes from "prop-types";
import { DropTarget } from "react-dnd";

import DraggableTypes from "../../../domain/model/DraggableTypes";

import DeckHeader from "./DeckHeader";
import DeckContent from "./DeckContent";

import styles from "./Deck.scss";

// Target specifications
const deckTarget = {
  drop(props, monitor) {
    const droppedItem = monitor.getItem();
    props.onCardDrop(droppedItem.originalDeckId, props.deck.id, droppedItem.cardId, droppedItem.index);
  },
  hover(props, monitor, component) {
    const movedItem = monitor.getItem();
    const clientOffset = monitor.getClientOffset();

    let newIndex = movedItem.index;

    // Same deck
    if (props.deck.id === movedItem.deckId) {
      const triggerBoundaries = Array.from(component.rootEl.getElementsByClassName("box")) // find all our cards
        .map((el) => el.getBoundingClientRect()) // get all rects
        .map((rect) => rect.y + rect.height / 2 ) // get middle line to rect
        .slice(Math.max(movedItem.index - 1, 0), movedItem.index + 2); // get our element boundaries

      if (movedItem.index > 0 && clientOffset.y < Math.min(...triggerBoundaries)) {
        --newIndex;
      }
      else if (movedItem.index < props.deck.cards.length && clientOffset.y > Math.max(...triggerBoundaries)) {
        ++newIndex;
      }
      else {
        return;
      }
    }
    else { // Different decks
      newIndex = Array.from(component.rootEl.getElementsByClassName("box")) // find all our cards
        .map((el) => el.getBoundingClientRect()) // get all rects
        .map((rect) => rect.y + rect.height / 2 ) // get middle line to rect
        .filter((midPos) => midPos <= clientOffset.y) // Keep only those below mouse pointer
        .length; // this gives us our new index
    }

    props.onCardMove(movedItem.deckId, props.deck.id, movedItem.cardId, newIndex);
    // !! Mutations
    monitor.getItem().deckId = props.deck.id;
    monitor.getItem().index = newIndex;
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

/**
 * A Deck in the app
 * @param {*} props
 */
class Deck extends React.Component {
  render() {
    const deckClassNames = styles.Deck + " card";
    return this.props.connectDropTarget(
      <div ref={(rootEl) => this.rootEl = rootEl} className={deckClassNames}>
        <DeckHeader
          name={this.props.deck.name}
          count={this.props.deck.cards.length} />
        <DeckContent
          id={this.props.deck.id}
          cards={this.props.deck.cards}
          isOver={this.props.isOver} />
      </div>
    );
  }
}

Deck.propTypes = {
  deck: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    cards: PropTypes.array.isRequired,
  }).isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  isOver: PropTypes.bool.isRequired,
  onCardDrop: PropTypes.func.isRequired,
  onCardMove: PropTypes.func.isRequired
};

export default DropTarget(DraggableTypes.CARD, deckTarget, collect)(Deck);
