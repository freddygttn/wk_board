import React from "react";
import PropTypes from "prop-types";

/**
 * Header of a Deck
 * @param {*} props
 */
const DeckHeader = ({ name, count }) => (
  <header className="card-header">
    <p className="card-header-title is-uppercase">
      <span>{name}</span>
      <span>&nbsp;</span>
      <span className="tag is-light">{count}</span>
    </p>
    <a href="#" className="card-header-icon has-text-grey-light" aria-label="more options">
      <span className="icon">
        <i className="fas fa-angle-down" aria-hidden="true"></i>
      </span>
    </a>
  </header>
);

DeckHeader.propTypes = {
  name: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
};

export default DeckHeader;
