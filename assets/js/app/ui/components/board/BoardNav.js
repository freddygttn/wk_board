import React from "react";
import PropTypes from "prop-types";

import styles from "./BoardNav.scss";
import Indicator from "../indicator/Indicator";

const BoardNav = ({ name, status }) => {
  const boardNavClassNames = styles.BoardNav + " navbar is-link";
  const menuClassNames = styles.BoardNavMenu + " navbar-item";
  return (
    <nav className={boardNavClassNames} role="navigation" aria-label="secondary navigation">
      <div className="navbar-brand">
        <a className={menuClassNames} href="#">
          <span className="icon">
            <i className="fas fa-bars"></i>
          </span>
        </a>
        <div className="navbar-item">
          <Indicator status={status} />
        </div>
        <div className="navbar-item">
          {name}
        </div>
      </div>
    </nav>
  );
};

BoardNav.propTypes = {
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired
};

export default BoardNav;
