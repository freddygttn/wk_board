import React from "react";
import PropTypes from "prop-types";

import BoardNav from "./BoardNav";
import BoardContent from "./BoardContent";

const Board = ({ name, decks, status }) => {
  return (
    <div className="wk-board">
      <BoardNav name={name} status={status} />
      <BoardContent decks={decks} />
    </div>
  );
};

Board.propTypes = {
  name: PropTypes.string.isRequired,
  decks: PropTypes.array.isRequired,
  status: PropTypes.string.isRequired
};

export default Board;
