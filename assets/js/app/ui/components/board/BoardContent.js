import React from "react";
import PropTypes from "prop-types";

import BoardDeck from "../../containers/BoardDeck";
import styles from "./BoardContent.scss";

const BoardContent = ({ decks }) => (
  <div className={styles.BoardContent}>
    {decks.map((deck, index) => (
      <BoardDeck key={index} deck={deck} />
    ))}
  </div>
);

BoardContent.propTypes = {
  decks: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      cards: PropTypes.array.isRequired
    })
  ).isRequired
};

export default BoardContent;
