/**
 * Socket utilitaries.
 */
import { SocketEvents } from "../domain/model/SocketEvents";

/**
 * Forward a payload from an callback to a subject.
 * @param {*} subject The subject to send the event to
 * @param {*} event The event type to be sent
 */
export function createPayloadCallback(subject, event) {
  return (payload) => {
    subject.next({
      event,
      payload
    });
  };
}

/**
 * Send an event though a channel and forward the response through a subject.
 * Usefull if expecting a response from a socket
 * @param {*} channel The channel to send the event to
 * @param {*} subject The subject that will forward the response
 * @param {*} event The event type to send
 * @param {*} params Params needed by the event
 * @param {*} timeout Timeout of the request (defaults to 10000)
 */
export function sendReplyEvent(channel, subject, event, params, timeout = 10000) {
  channel.push(event, params, timeout)
    .receive(SocketEvents.Response.Ok, createPayloadCallback(subject, SocketEvents.EventDelivery.Ok))
    .receive(SocketEvents.Response.Error, createPayloadCallback(subject, SocketEvents.EventDelivery.Error))
    .receive(SocketEvents.Response.Timeout, createPayloadCallback(subject, SocketEvents.EventDelivery.Timeout));
}
