/**
 * Statefull module to handle socket connection.
 */
import { Socket } from "phoenix";
import { Subject } from "rxjs/Subject";
import boardChannel from "./board";
import SocketEvents from "../domain/model/SocketEvents";

const BOARD_ID = "wk-default-board";
const socketStatusSubject = new Subject();
let conn = null;
let board = null;

/**
 * Get a connection to the socket.
 * Returns an object containing a subject that feeds information about
 * the socket state.
 * @param {*} resetConnection A boolean to reset the connection
 */
export function connect(resetConnection = false) {
  // Reset connection if needed
  if (conn !== null && resetConnection) {
    conn.disconnect();
    conn = null;
    board = null;
  }

  // Initialize connection
  if (conn === null) {
    conn = new Socket("/socket", {params: {}});

    conn.onOpen((payload) => {
      socketStatusSubject.next({
        event: SocketEvents.Socket.Open,
        payload
      });
    });

    conn.onClose((payload) => {
      socketStatusSubject.next({
        event: SocketEvents.Socket.Close,
        payload
      });
    });

    conn.onError((payload) => {
      socketStatusSubject.next({
        event: SocketEvents.Socket.Error,
        payload
      });
    });

    conn.connect();
  }

  return socketStatusSubject;
}

/**
 * Get or create a board channel.
 * Throw if connection is not initialized.
 */
export const getBoardChannel = () => {
  if (conn === null) {
    throw("Initialize socket first via connect function");
  }
  if (board === null) {
    board = boardChannel(conn, BOARD_ID);
  }

  return board;
};
