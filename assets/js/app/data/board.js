import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import { createPayloadCallback } from "./socket-utils";
import SocketEvents from "../domain/model/SocketEvents";

/**
 * Manages a Board channel. All incoming events will be feeded to a subject.
 * Returns an object containing a function to listen to the subject,
 * functions to join and leave the channel
 * and other functions to send events to the server.
 * @param {*} socket The socket to connect
 * @param {*} boardId The board id to use
 * @param {*} params Params that would be needed to join the socket
 */
export default function (socket, boardId, params = {}) {
  let isJoined = false;
  let joinPayload = {};
  const channel = socket.channel(`board:${boardId}`, params);
  const incomingEventsSubject = new Subject();

  channel.on(SocketEvents.Board.CardMoved, createPayloadCallback(incomingEventsSubject, SocketEvents.Board.CardMoved));

  return {
    asObservable: () => incomingEventsSubject,
    moveCard: (from_deck_id, to_deck_id, card_id, card_index) => {
      channel.push(SocketEvents.Board.CardMove, {from_deck_id, to_deck_id, card_id, card_index}, 3000);
    },
    join: () => {
      if (isJoined) {
        return Observable.of({event: SocketEvents.ChannelJoin.Ok, payload: joinPayload});
      }
      return Observable.create((observer) => {
        channel.join()
          .receive(SocketEvents.Response.Ok, (data) => {
            isJoined = true;
            joinPayload = data;
            observer.next({event: SocketEvents.ChannelJoin.Ok, payload: data});
            observer.complete();
          })
          .receive(SocketEvents.Response.Error, (reason) => {
            observer.next({event: SocketEvents.ChannelJoin.Error, payload: reason});
            observer.complete();
          });
      });
    },
    leave: () => {
      return Observable.create((observer) => {
        channel.leave(3000) // timeout
          .receive(SocketEvents.Response.Ok, (data) => {
            isJoined = false;
            joinPayload = {};
            observer.next(data);
            observer.complete();
          })
          .receive(SocketEvents.Response.Error, (reason) => {
            observer.error(reason);
            observer.complete();
          })
          .receive(SocketEvents.Response.Timeout, () => {
            observer.error({reason: "Leave channel timeout"});
            observer.complete();
          });
      });
    }
  };
}
