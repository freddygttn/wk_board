import { moveToDeck, reorderOnly } from "../../../../../../js/app/domain/reducers/board/reducer";

const initialState = {
  id: "board0",
  decks: [
    {
      id: "deck0",
      cards: [{ id: "card0" }]
    },
    {
      id: "deck1",
      cards: []
    }
  ]
};

describe("moveToDeck", () => {

  it("should move a card from one deck to another", () => {
    const expectedState = {
      id: "board0",
      decks: [
        {
          id: "deck0",
          cards: []
        },
        {
          id: "deck1",
          cards: [{ id: "card0" }]
        }
      ]
    };

    expect(moveToDeck(initialState, "deck0", "deck1", "card0")).toEqual(expectedState);
  });

  it("should return initial state if fromDeckId is not found in board", () => {
    expect(moveToDeck(initialState, "deck10", "deck1", "card0")).toEqual(initialState);
  });

  it("should return initial state if toDeckId is not found in board", () => {
    expect(moveToDeck(initialState, "deck0", "deck10", "card0")).toEqual(initialState);
  });

  it("should return initial state if cardId is not found in fromDeckId", () => {
    expect(moveToDeck(initialState, "deck0", "deck1", "card10")).toEqual(initialState);
  });

  it("should reorder if fromDeckId is the same as toDeckId", () => {
    expect(moveToDeck(initialState, "deck0", "deck0", "card0")).toEqual(initialState);
  });

});

describe("reorderOnly", () => {
  it("should reorder the card", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }, { id: "card1" }, { id: "card2" }]
    };

    const expectedDeck = {
      id: "deck0",
      cards: [{ id: "card2" }, { id: "card0" }, { id: "card1" }]
    };

    expect(reorderOnly(initialDeck, "card2", 0)).toEqual(expectedDeck);
  });

  it("should not reorder the card if the index is the same", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }, { id: "card1" }, { id: "card2" }]
    };

    expect(reorderOnly(initialDeck, "card2", 2)).toEqual(initialDeck);
  });

  it("should do nothing if there is only one card", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }]
    };

    expect(reorderOnly(initialDeck, "card0", 0)).toEqual(initialDeck);
  });

  it("should do nothing if there are no card", () => {
    const initialDeck = {
      id: "deck0",
      cards: []
    };

    expect(reorderOnly(initialDeck, "card0", 0)).toEqual(initialDeck);
  });

  it("should do nothing if the id is invalid", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }]
    };

    expect(reorderOnly(initialDeck, "card1", 0)).toEqual(initialDeck);
  });

  it("should have the same result if called twice in a row", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }, { id: "card1" }, { id: "card2" }]
    };

    const expectedDeck = {
      id: "deck0",
      cards: [{ id: "card2" }, { id: "card0" }, { id: "card1" }]
    };

    const firstResult = reorderOnly(initialDeck, "card2", 0);
    const secondResult = reorderOnly(firstResult, "card2", 0);

    expect(firstResult).toEqual(expectedDeck);
    expect(secondResult).toEqual(firstResult);
  });


  it("should set invalid index to length - 1", () => {
    const initialDeck = {
      id: "deck0",
      cards: [{ id: "card0" }, { id: "card1" }, { id: "card2" }]
    };

    const expectedDeck = {
      id: "deck0",
      cards: [{ id: "card0" }, { id: "card1" }, { id: "card2" }]
    };
    const expectedDeckBis = {
      id: "deck0",
      cards: [{ id: "card1" }, { id: "card2" }, { id: "card0" }]
    };

    expect(reorderOnly(initialDeck, "card2", null)).toEqual(expectedDeck);
    expect(reorderOnly(initialDeck, "card2", -1)).toEqual(expectedDeck);
    expect(reorderOnly(initialDeck, "card2", 3)).toEqual(expectedDeck);
    expect(reorderOnly(initialDeck, "card0", null)).toEqual(expectedDeckBis);
    expect(reorderOnly(initialDeck, "card0", -1)).toEqual(expectedDeckBis);
    expect(reorderOnly(initialDeck, "card0", 3)).toEqual(expectedDeckBis);
  });
});
