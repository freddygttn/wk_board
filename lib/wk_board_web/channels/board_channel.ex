defmodule WkBoardWeb.BoardChannel do
  @moduledoc """
  Handles incoming channel connections and messages for boards.

  ## Incoming events (**Sent** by client) and params

    - card:move
      - from_deck_id: *String* Previous card deck id (required)
      - to_deck_id: *String* New card deck id (required)
      - card_id: *String* Id of the card to move (required)

  ## Outgoing events (Broadcast, **received** by client)

    - card:moved (from_deck_id, to_deck_id, card_id)
  """
  use WkBoardWeb, :channel

  @doc """
  Handles joins.
  """
  def join("board:" <> board_id, _params, socket) do
    {:ok, pid} = WkBoard.Boards.BoardsSupervisor.find_or_create_board(board_id)
    board = WkBoard.Boards.BoardServer.get_board(pid)

    {:ok, %{board: WkBoard.Protocols.FrontJson.toFrontJson(board)}, socket}
  end

  @doc """
  Handles incoming messages.
  """
  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (board:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # Handles card move.
  def handle_in("card:move", %{"from_deck_id" => from_deck_id, "to_deck_id" => to_deck_id, "card_id" => card_id, "card_index" => card_index}, socket) do
    "board:" <> board_id = socket.topic
    with {:ok, pid} <- WkBoard.Boards.BoardsSupervisor.find_or_create_board(board_id) do
      WkBoard.Boards.BoardServer.move_card(pid, from_deck_id, to_deck_id, card_id, card_index)
      broadcast! socket, "card:moved", %{from_deck_id: from_deck_id, to_deck_id: to_deck_id, card_id: card_id, card_index: card_index}
    end

    {:noreply, socket}
  end

  # Handles card move with no index.
  def handle_in("card:move", %{"from_deck_id" => from_deck_id, "to_deck_id" => to_deck_id, "card_id" => card_id}, socket) do
    "board:" <> board_id = socket.topic
    with {:ok, pid} <- WkBoard.Boards.BoardsSupervisor.find_or_create_board(board_id) do
      WkBoard.Boards.BoardServer.move_card(pid, from_deck_id, to_deck_id, card_id, 0)
      broadcast! socket, "card:moved", %{from_deck_id: from_deck_id, to_deck_id: to_deck_id, card_id: card_id, card_index: 0}
    end

    {:noreply, socket}
  end

end
