defmodule WkBoardWeb.PageController do
  use WkBoardWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
