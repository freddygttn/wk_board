defmodule WkBoard.Boards.Supervisor do
  @moduledoc """
  Supervises boards registry and boards supervisor.
  """
  use Supervisor

  @registry_name_boards :boards_registry

  @doc """
  Starts the supervisor.
  """
  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @doc """
  Initializes the supervisor.
  """
  def init(:ok) do
    children = [
      {Registry, [keys: :unique, name: @registry_name_boards]},
      WkBoard.Boards.BoardsSupervisor
    ]

    # one_for_all: The supervisor will kill and restart all of its children processes whenever any one of them dies
    Supervisor.init(children, strategy: :one_for_all)
  end
end
