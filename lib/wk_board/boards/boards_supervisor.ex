defmodule WkBoard.Boards.BoardsSupervisor do
  @moduledoc """
  Supervisor to handle the creation of dynamic `WkBoard.Boards.BoardServer` processes using a
  `simple_one_for_one` strategy. See the `init` callback at the bottom for details on that.

  These processes will spawn for each `board_id` provided to the `WkBoard.Boards.BoardServer.start_link` function.

  Functions contained in this supervisor module will assist in the creation and retrieval of
  new board processes.

  Also note the guards utilizing `is_binary(board_id)` on the functions.
  If someone makes a mistake and tries sending a string-based key or an atom, it will just _"let it crash"_.
  """
  use Supervisor

  @registry_name_boards :boards_registry

  # registry lookup handler
  defp via_tuple(board_id), do: {:via, Registry, {@registry_name_boards, board_id}}

  @doc """
  Starts the supervisor.
  """
  def start_link(_opts) do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @doc """
  Will find the process identifier (in our case, the `board_id`) if it exists in the registry and
  is attached to a running `WkBoard.Boards.BoardServer` process.
  If the `board_id` is not present in the registry, it will create a new `WkBoard.Boards.BoardServer`
  process and add it to the registry for the given `board_id`.

  Returns a tuple such as `{:ok, pid}` or `{:error, reason}`
  """
  def find_or_create_board(board_id) when is_binary(board_id) do
    if board_exists?(board_id) do
      [{pid, _}] = Registry.lookup(@registry_name_boards, board_id)
      {:ok, pid}
    else
      board_id |> create_board
    end
  end

  @doc """
  Determines if a `WkBoard.Boards.BoardServer` process exists, based on the `board_id` provided.

  Returns a boolean.

  ## Example

      iex> WkBoard.Boards.BoardsSupervisor.board_exists?("unknown_id")
      false

  """
  def board_exists?(board_id) when is_binary(board_id) do
    case Registry.lookup(@registry_name_boards, board_id) do
      [] -> false
      _ -> true
    end
  end

  @doc """
  Creates a new `WkBoard.Boards.BoardServer` process, based on the `board_id` string.

  Returns a tuple such as `{:ok, pid}` if successful.
  If there is an issue, an `{:error, reason}` tuple is returned.
  """
  def create_board(board_id) when is_binary(board_id) do
    case Supervisor.start_child(__MODULE__, id: board_id, name: via_tuple(board_id)) do
      {:ok, pid} -> {:ok, pid}
      {:error, {:already_started, _pid}} -> {:error, :process_already_exists}
      other -> {:error, other}
    end
  end

  @doc """
  Returns the count of `WkBoard.Boards.BoardServer` processes managed by this supervisor.
  ## Example
      iex> WkBoard.Boards.BoardsSupervisor.board_count
      0
  """
  def board_count, do: __MODULE__ |> Supervisor.which_children |> length

  @doc """
  Initializes the supervisor.
  """
  def init(:ok) do
    children = [
      WkBoard.Boards.BoardServer
    ]

    # With this strategy, no workers are started during the supervisor initialization,
    # and a new worker is started each time start_child/2 is called.
    Supervisor.init(children, strategy: :simple_one_for_one)
  end
end
