defmodule WkBoard.Boards.BoardServer do
  @moduledoc """
  Stores and manage board data.
  """
  use GenServer, restart: :temporary
  require Logger

  alias WkBoard.Model.Board, as: Board
  alias WkBoard.Model.Deck, as: Deck
  alias WkBoard.Model.Card, as: Card

  # ---------------------------------------------------------------------------
  ## Client API
  # ---------------------------------------------------------------------------
  @doc """
  Starts a new board for a given `board_id` and assigns its `name`.
  The name could be a :via tuple to make it available in the registry.
  """
  def start_link(_opts, {:id, board_id}, {:name, name}) do
    GenServer.start_link(__MODULE__, [board_id], name: name)
  end
  # For testing purposes
  def start_link(_opts) do
    GenServer.start_link(__MODULE__, ["test"], name: nil)
  end

  @doc """
  Stops a board.
  """
  def stop(server) do
    GenServer.stop(server)
  end

  @doc """
  Gets bord information. (Call)

  Returns a `WkBoard.Model.Board`
  """
  def get_board(server) do
    GenServer.call(server, {:get_board})
  end

  @doc """
  Adds a deck to the board. (Cast)
  """
  def add_deck(server, deck) do
    GenServer.cast(server, {:add_deck, deck})
  end

  @doc """
  Adds a card to a deck in the board. (Cast)
  """
  def add_card(server, deck_id, card) do
    GenServer.cast(server, {:add_card, deck_id, card})
  end

  @doc """
  Moves a card between two decks. (Cast)
  """
  def move_card(server, from_deck_id, to_deck_id, card_id, card_index) do
    GenServer.cast(server, {:move_card, from_deck_id, to_deck_id, card_id, card_index})
  end


  # ---------------------------------------------------------------------------
  ## Server Callbacks
  # ---------------------------------------------------------------------------
  @doc """
  Initializes the server.
  """
  def init([board_id]) do

    # Add a msg to the process mailbox to tell this process to run `:init_data`
    send(self(), :init_data)
    Logger.info("Process created with board_id: #{board_id}")

    # Set initial state and return from `init`
    {:ok, %Board{board_id: board_id}}
  end

  @doc """
  Callback handler to create some default data.
  """
  def handle_info(:init_data, state) do
    card0 = %Card{
      card_id: "card0",
      name: "Steve Jobs",
      content: "Producteur de pommes",
      picture_url: "https://upload.wikimedia.org/wikipedia/commons/f/f5/Steve_Jobs_Headshot_2010-CROP2.jpg",
      stats: %{
        rating: 3.6,
        likes: 5,
        msgs: 6,
        emails: 5
      }
    }

    card1 = %Card{
      card_id: "card1",
      name: "Jean Michel",
      content: "Artiste aux multiples talents",
      picture_url: "https://i.ytimg.com/vi/vQUaAGB33W8/hqdefault.jpg",
      stats: %{
        rating: 4.1,
        likes: 7,
        msgs: 12,
        emails: 3
      }
    }

    card2 = %Card{
      card_id: "card2",
      name: "Richard Stallman",
      content: "Distributeur de bières gratuites",
      picture_url: "https://images.techhive.com/images/article/2015/05/richard-stallman-100586957-primary.idge.jpg",
      stats: %{
        rating: 4.4,
        likes: 14,
        msgs: 2,
        emails: 11
      }
    }

    deck0 = %Deck{
      deck_id: "deck0",
      name: "À rencontrer",
      cards: [card0, card1]
    }

    deck1 = %Deck{
      deck_id: "deck1",
      name: "Entretien",
      cards: [card2]
    }

#    deck2 = %Deck{
#      deck_id: "deck2",
#      name: "En test technique",
#      cards: []
#    }

    updated_state = state
    |> Map.put(:name, "Stage - Account Manager")
    |> Map.put(:decks, [deck0, deck1])


    {:noreply, updated_state}
  end

  def handle_info(_, state), do: {:noreply, state}

  # Call Handlers
  # ---------------------------------------------------------------------------
  @doc """
  Handles get board.
  """
  def handle_call({:get_board}, _from, state) do
    {:reply, state, state}
  end

  # Cast Handlers
  # ---------------------------------------------------------------------------
  @doc """
  Handles add deck.
  """
  def handle_cast({:add_deck, deck}, state) do
    updated_state = %Board{state | decks: state.decks ++ [deck]}

    {:noreply, updated_state}
  end

  @doc """
  Handles add card.
  """
  def handle_cast({:add_card, deck_id, card}, state) do
    decks = state.decks
    |> Enum.map(fn(deck) ->
      if deck.deck_id == deck_id do
        deck |> Map.put(:cards, deck.cards ++ [card])
      else
        deck
      end
    end)

    updated_state = %Board{state | decks: decks}

    {:noreply, updated_state}
  end

  @doc """
  Handles move card.
  """
  def handle_cast({:move_card, from_deck_id, to_deck_id, card_id, card_index}, state) do

    card = state.decks
    |> Enum.find(nil, fn(deck) -> deck.deck_id == from_deck_id end)
    |> Map.get(:cards, [])
    |> Enum.find(nil, fn(card) -> card.card_id == card_id end)

    decks = do_move_card(state, from_deck_id, to_deck_id, card, card_index)

    {:noreply, %Board{state | decks: decks}}
  end

  defp do_move_card(state, _, _, nil, _), do: state.decks

  defp do_move_card(state, from_deck_id, to_deck_id, card, card_index) when from_deck_id == to_deck_id do
    if card != nil do
      state.decks
      |> Enum.map(fn(deck) ->
        cond do
          deck.deck_id == from_deck_id -> reorder_cards(deck, card, card_index)
          true -> deck
        end
      end)
    else
      state.decks
    end
  end

  defp do_move_card(state, from_deck_id, to_deck_id, card, card_index) do
    if card != nil do
      state.decks
      |> Enum.map(fn(deck) ->
        cond do
          deck.deck_id == from_deck_id -> %Deck{deck | cards: deck.cards |> Enum.filter(fn(c) -> c.card_id != card.card_id end)}
          deck.deck_id == to_deck_id -> reorder_cards(%Deck{deck | cards: deck.cards ++ [card]}, card, card_index)
          true -> deck
        end
      end)
    else
      state.decks
    end
  end

  defp reorder_cards(deck, card, index) when is_integer(index) and index >= 0 do
    card_index = deck.cards
      |> Enum.find_index(fn(c) -> c.card_id == card.card_id end)

    new_cards = cond do
      card_index < index -> deck.cards |> List.insert_at(index, card) |> List.delete_at(index)
      card_index > index -> deck.cards |> List.insert_at(index, card) |> List.delete_at(index + 1)
      card_index == index -> deck.cards
    end

    %Deck{deck | cards: new_cards}
  end

  defp reorder_cards(deck, _, _), do: deck

end
