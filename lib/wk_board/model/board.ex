defmodule WkBoard.Model.Board do
  @moduledoc """
  Represents a Board.
  """
  defstruct board_id: "",
            name: "",
            decks: []
end