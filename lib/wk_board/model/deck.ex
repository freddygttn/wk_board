defmodule WkBoard.Model.Deck do
  @moduledoc """
  Represents a Deck in a Board.
  """
  defstruct deck_id: "",
            name: "",
            cards: []
end