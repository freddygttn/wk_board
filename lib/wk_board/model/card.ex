defmodule WkBoard.Model.Card do
  @moduledoc """
  Represents a Card in a Deck.
  """
  defstruct card_id: "",
            name: "",
            content: "",
            picture_url: "",
            stats: %{
              rating: 0,
              likes: 0,
              msgs: 0,
              emails: 0
            }
end