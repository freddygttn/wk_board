defprotocol WkBoard.Protocols.FrontJson do
  @doc "Transform model data for front-end"
  def toFrontJson(data)
end

defimpl WkBoard.Protocols.FrontJson, for: WkBoard.Model.Board do
  def toFrontJson(board) do
    %{
      "id" => board.board_id,
      "name" => board.name,
      "decks" => board.decks |> Enum.map(fn(deck) -> WkBoard.Protocols.FrontJson.toFrontJson(deck) end)
    }
  end
end

defimpl WkBoard.Protocols.FrontJson, for: WkBoard.Model.Deck do
  def toFrontJson(deck) do
    %{
      "id" => deck.deck_id,
      "name" => deck.name,
      "cards" => deck.cards |> Enum.map(fn(card) -> WkBoard.Protocols.FrontJson.toFrontJson(card) end)
    }
  end
end

defimpl WkBoard.Protocols.FrontJson, for: WkBoard.Model.Card do
  def toFrontJson(card) do
    %{
      "id" => card.card_id,
      "name" => card.name,
      "content" => card.content,
      "pictureUrl" => card.picture_url,
      "stats" => %{
        "rating" => card.stats.rating,
        "likes" => card.stats.likes,
        "msgs" => card.stats.msgs,
        "emails" => card.stats.emails
      }
    }
  end
end