# WK Board

Realtime drag and drop of cards on a Trello like board.

## Usage

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install` or `cd assets && yarn` 
  * Start Phoenix endpoint with `mix phx.server`
  
Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


## Stack

  * Phoenix (Elixir), simple and efficient
  * Brunch, comes with Phoenix generators, should be enough for this project so keep it simple
  * React, combined with Redux for better state management


## Libraries

  * ramda for its usefull pure functions
  * SCSS and Bulma for styling (also using CSS modules)
  * rxjs and redux-observables to easily implement async actions
  * react-dnd for painless drag and drop capabilities
  * Jest for testing
  * Eslint (js) / Credo (elixir) for linting


## Design choices

### Front-end

The web app is split into three parts : data, domain and ui.
Data provides ways to handle external data, like our socket connection.
Ui contains everything that will be rendered, mostly containers and components. 
Last, domain holds our core logic with reducers and epics. It makes the link between
our data sources and the ui.

The data managed by the app is like a Trello board.
A board contains some decks which contains some cards.
This hierarchical kind of structure fit perfectly within a json object and therefore within a reducer.
Lastly, a second reducer is used to manage socket connectivity. 

### Back-end

Data is stored using a GenServer. Since the idea would be to handle multiple boards,
each GenServer pid needs to be accessible using a board id.
The Registry module provided by Phoenix is perfect for this.

However, there is one problem: what if a GenServer crashes? And what if the Registry crashes?
To handle this, two supervisors are added, one to handle our GenServers,
and the other to handle the Registry and the first supervisor.

Finally, the model could easily be updated with Ecto to handle a database for persistence.

App structure follows Phoenix generators, which is easily maintainable and scalable.


## TODO

### Front-end

##### Data

- Error handling in epics
- Improve connection management

##### Domain

- Handles card not moved by the server (only *really* optimistic update from now, no going back!)
- Split moveToDeck function, it does too many things
- Rethink actions to clarify their usage (maybe add a DRAG_START action)
- Add flow to provide type safety

##### UI

- Enhance styling
- Cards needs to be split into smaller components
- Deck's DropTarget logic needs to be extracted to be tested
- Improve Deck's DropTarget logic to be more flexible

### Back-end

- Add response from the channel and maybe use a call instead of a cast for state update
(since this kind of app does not need extreme availability, better to make it consistent)
- Improve BoardServer code (Maybe extract state functions into a new module)
- Test BoardServer
- Test BoardChannel

### Project

- Add jsdoc / ex_doc and provide a better documentation
- Dockerfile (distillery for elixir may be needed)


## Last words

Even though lots of things needs to be done (there always will be), this app is in a functioning 
state and can be easily improved.

Now would be a good starting point to use more GitLab features 
like issues or the wiki.